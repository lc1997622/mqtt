import Vue from 'vue'
import Router from 'vue-router'
import menus from '@/config/menu-config'
import personShow from '@/components/PersonShow'
import personInfo from '@/components/PersonInfo'
import login from '@/components/Login'

Vue.use(Router)

var routes = []

menus.forEach((item) => {
    item.sub.forEach((sub) => {
        routes.push({
            path: `/${sub.componentName}`,
            name: sub.componentName,
            component: () =>
                import (`@/components/${sub.componentName}`)
        })
    })
})

routes.push({
    path: '/PersonShow',
    name: 'PersonShow',
    component: personShow,
})

routes.push({
    path: '/PersonInfo',
    name: 'PersonInfo',
    component: personInfo,
})
routes.push({
    path: '/',
    name: 'home',
    redirect: '/login'
})
routes.push({
    path: '/login',
    name: 'login',
    component: login
})


export default new Router({ routes })