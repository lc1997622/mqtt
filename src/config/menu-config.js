module.exports = [{
    name: '疫情数据管理',
    id: 'disease',
    sub: [{
        name: '实时数据',
        componentName: 'DiseaseReal'
    }, {
        name: '历史数据',
        componentName: 'DiseaseHistory'
    }]
}, {
    name: '温度数据管理',
    id: 'health',
    sub: [{
        name: '实时数据',
        componentName: 'HealthReal'
    }, {
        name: '历史数据',
        componentName: 'HealthHistory'
    }]
}, {
    name: '位置数据管理',
    id: 'location',
    sub: [{
        name: '位置数据',
        componentName: 'Location'
    }]
}, {
    name: '片区人员健康档案',
    id: 'healthFile',
    sub: [{
        name: '片区健康档案',
        componentName: 'HealthFile'
    }]
}, {
    name: '应急处理中心',
    id: 'yingJi',
    sub: [{
        name: '应急处理中心',
        componentName: 'YingJi'
    }]
}]